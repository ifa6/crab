package com.baomidou.crab.v1.mapper;

import com.baomidou.crab.v1.entity.Link;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 友情链接表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-02-07
 */
public interface LinkMapper extends BaseMapper<Link> {

}
